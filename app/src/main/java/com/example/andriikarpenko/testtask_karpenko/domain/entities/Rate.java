package com.example.andriikarpenko.testtask_karpenko.domain.entities;


public class Rate {

    private String from;
    private String to;
    private float rate;

    public Rate() {
    }

    public Rate(String from, String to, float rate) {
        this.from = from;
        this.to = to;
        this.rate = rate;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public float getRate() {
        return rate;
    }
}
