package com.example.andriikarpenko.testtask_karpenko.presenters;

import com.example.andriikarpenko.testtask_karpenko.domain.repository.ResourceRepository;
import com.example.andriikarpenko.testtask_karpenko.domain.usecases.GetProductsUseCase;
import com.example.andriikarpenko.testtask_karpenko.domain.usecases.GetTransactionsUseCase;
import com.example.andriikarpenko.testtask_karpenko.ui.MainView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainPresenter implements Presenter<MainView> {

    private MainView mView;
    private CompositeDisposable disposables = new CompositeDisposable();
    private String currentProduct;

    @Override
    public void attachView(MainView view) {
        mView = view;
    }

    @Override
    public void onStart() {
        mView.showProgress(true);
        Disposable mProductDisposable = new GetProductsUseCase(ResourceRepository.getInstance()).execute()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(strings -> {
                    mView.showProgress(false);
                    mView.setProducts(strings);
                });
        disposables.add(mProductDisposable);
    }

    @Override
    public void onStop() {
        if (!disposables.isDisposed()) {
            disposables.dispose();
            disposables.clear();
        }
    }

    public void onProductSelected(String product) {
        mView.showProgress(true);
        mView.hideProductInfo();

        Disposable mTransactionDisposable = new GetTransactionsUseCase(ResourceRepository.getInstance(), product, "GBP").execute()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(transactions -> {
                    currentProduct = product;
                    mView.showProgress(false);
                    mView.showProductInfo(product, transactions.size());
                });
        disposables.add(mTransactionDisposable);
    }

    public void onProductClicked() {
        mView.openProductDetails(currentProduct);
    }

}
