package com.example.andriikarpenko.testtask_karpenko.utils;

import com.example.andriikarpenko.testtask_karpenko.domain.entities.Rate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class CurrencyConverter {

    private List<Rate> rates;

    public CurrencyConverter(List<Rate> rates) {
        this.rates = rates;
    }

    public float convert(float amount, String from, String to) {
        List<Rate> chain = new ArrayList<>();

        if (from.equals(to)) return amount;

        chain = findRate(createDictionary(rates), chain, from, to);
        return calculateAmount(chain, amount);
    }

    private float calculateAmount(List<Rate> rates, float amount) {
        if (rates == null) throw new ArithmeticException("Cannot calculate");

        for (Rate rate: rates) {
            amount *= rate.getRate();
        }
        return amount;
    }

    private List<Rate> findRate(Map<String, List<Rate>> dictionary, List<Rate> currentChain, String from, String to) {

        List<Rate> newRates = dictionary.get(from);
        Rate newWay = null;

        if (newRates != null) {
            //found needed To
            for (Rate rate : newRates) {
                if (rate.getTo().equals(to)) {
                    currentChain.add(rate);
                    return currentChain;
                }
            }

            //check, could we move to next rate or way is wrong and we should move back

            for (Rate newRate : newRates) {
                boolean alreadyExist = false;
                for (Rate curRate : currentChain) {
                    if (curRate.getFrom().equals(newRate.getTo())) {
                        alreadyExist = true;
                        break;
                    }
                }
                if (!alreadyExist) {
                    newWay = newRate;
                    break;
                }
            }
        }

        if (newWay == null) {
            // no possible way, game over
            if (currentChain.size() == 0) {
                return null;
            }

            Rate lastInChain = currentChain.get(currentChain.size() -1);
            List<Rate> lastInDict = dictionary.get(lastInChain.getFrom());
            String previousFrom = lastInChain.getFrom();

            //remove from dictionary wrong way;
            for (Iterator<Rate> iterator = lastInDict.iterator(); iterator.hasNext();) {
                Rate rate = iterator.next();
                if (lastInChain.getTo().equals(rate.getTo())) {
                    lastInDict.remove(rate);
                    break;
                }
            }

            //remove from chain wrong way
            currentChain.remove(lastInChain);
            return findRate(dictionary, currentChain, previousFrom, to);
        } else {
            //found new way, search next possible way
            currentChain.add(newWay);
            return findRate(dictionary, currentChain, newWay.getTo(), to);
        }

    }

    private Map<String, List<Rate>> createDictionary(List<Rate> rates) {
        Map<String, List<Rate>> dictionary = new HashMap<>();
        for (Rate rate : rates) {
            List<Rate> list = dictionary.get(rate.getFrom());
            if (list == null) {
                list = new ArrayList<>();
            }
            list.add(rate);
            dictionary.put(rate.getFrom(), list);
        }
        return dictionary;
    }

}
