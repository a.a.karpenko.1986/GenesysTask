package com.example.andriikarpenko.testtask_karpenko.presenters;

import com.example.andriikarpenko.testtask_karpenko.domain.repository.ResourceRepository;
import com.example.andriikarpenko.testtask_karpenko.domain.usecases.GetTransactionsUseCase;
import com.example.andriikarpenko.testtask_karpenko.ui.DetailsView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class DetailsPresenter implements Presenter<DetailsView> {

    private DetailsView mView;
    private Disposable mTransactionDisposable;

    @Override
    public void attachView(DetailsView view) {
        mView = view;
    }

    @Override
    public void onStart() {
        mView.showProgress(true);
        String product = mView.getProduct();
        mTransactionDisposable = new GetTransactionsUseCase(ResourceRepository.getInstance(), product, "GBP").execute()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(transactions -> {
                    mView.showProgress(false);
                    mView.setTransactions(transactions);
                });
    }

    @Override
    public void onStop() {
        if (mTransactionDisposable != null && !mTransactionDisposable.isDisposed()) {
            mTransactionDisposable.dispose();
        }
    }
}
