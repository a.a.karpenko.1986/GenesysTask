package com.example.andriikarpenko.testtask_karpenko.utils;

import com.example.andriikarpenko.testtask_karpenko.domain.entities.Transaction;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class TransactionPListParser {

    public static List<Transaction> parse(Document document) {
        final List<Transaction> dataModels = new ArrayList<>();

        final NodeList nodes_array = document.getElementsByTagName("array");

        for (int index = 0; index < nodes_array.getLength(); index++) {

            final Node node = nodes_array.item(index);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                final Element e = (Element) nodes_array.item(index);

                final NodeList nodeKey = e.getElementsByTagName("key");
                final NodeList nodeString = e.getElementsByTagName("string");
                Transaction transaction = new Transaction();

                for (int i = 0; i < nodeString.getLength(); i++) {

                    final Element eleKey = (Element) nodeKey.item(i);
                    final Element eleString = (Element) nodeString.item(i);

                    if (eleString != null) {
                        String strValue = getValue(eleString);

                        if (getValue(eleKey).equals("amount")) {
                            transaction = new Transaction();
                            transaction.setAmount(Float.valueOf(strValue));

                        } else if (getValue(eleKey).equals("currency")) {
                            transaction.setCurrency(strValue);

                        } else if (getValue(eleKey).equals("sku")) {
                            transaction.setSku(strValue);
                            dataModels.add(transaction);
                        }
                    }
                }
            }
        }
        return dataModels;
    }

    private static String getValue(Element item) {
        return item.getFirstChild().getNodeValue();
    }

}
