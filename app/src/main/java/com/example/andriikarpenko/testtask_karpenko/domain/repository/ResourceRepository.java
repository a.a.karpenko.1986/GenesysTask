package com.example.andriikarpenko.testtask_karpenko.domain.repository;

import com.example.andriikarpenko.testtask_karpenko.App;
import com.example.andriikarpenko.testtask_karpenko.utils.Utils;
import com.example.andriikarpenko.testtask_karpenko.domain.entities.Rate;
import com.example.andriikarpenko.testtask_karpenko.utils.RatePListParser;
import com.example.andriikarpenko.testtask_karpenko.domain.entities.Transaction;
import com.example.andriikarpenko.testtask_karpenko.utils.TransactionPListParser;

import org.w3c.dom.Document;

import java.io.IOException;
import java.util.List;

public class ResourceRepository implements Repository {

    private List<Transaction> transactions;
    private List<Rate> rates;

    private static ResourceRepository instance;

    public static ResourceRepository getInstance() {
        if (instance == null) {
            instance = new ResourceRepository();
        }
        return instance;
    }

    private ResourceRepository() {

    }

    public List<Transaction> getTransactions() {
        if (transactions == null) {
            Document document;
            try {
                document = Utils.getPlist(App.getContext(), "transactions.plist");
                transactions = TransactionPListParser.parse(document);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return transactions;
    }

    @Override
    public List<Rate> getRates() {

        if (rates == null) {
            Document document;
            try {
                document = Utils.getPlist(App.getContext(), "rates.plist");
                rates = RatePListParser.parse(document);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return rates;
    }


}
