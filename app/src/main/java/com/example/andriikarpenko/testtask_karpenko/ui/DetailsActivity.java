package com.example.andriikarpenko.testtask_karpenko.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.example.andriikarpenko.testtask_karpenko.R;
import com.example.andriikarpenko.testtask_karpenko.domain.entities.Transaction;
import com.example.andriikarpenko.testtask_karpenko.presenters.DetailsPresenter;

import java.util.List;

public class DetailsActivity extends AppCompatActivity implements DetailsView {

    public static final String PRODUCT_KEY = "PRODUCT_KEY";

    private ProgressBar progressBar;
    private TransactionAdapter mAdapter;

    private DetailsPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.transactions_list);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        RecyclerView rvTransactions = (RecyclerView) findViewById(R.id.rvTransactions);
        rvTransactions.setHasFixedSize(true);
        rvTransactions.setLayoutManager(new LinearLayoutManager(this));

        mAdapter = new TransactionAdapter();
        rvTransactions.setAdapter(mAdapter);

        presenter = new DetailsPresenter();
        presenter.attachView(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    @Override
    public void setTransactions(List<Transaction> transactions) {
        mAdapter.setItems(transactions);
    }

    @Override
    public String getProduct() {
        return getIntent().getStringExtra(PRODUCT_KEY);
    }

    @Override
    public void showProgress(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}
