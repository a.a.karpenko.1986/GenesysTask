package com.example.andriikarpenko.testtask_karpenko.ui;


import com.example.andriikarpenko.testtask_karpenko.domain.entities.Transaction;

import java.util.List;

public interface DetailsView {

    void setTransactions(List<Transaction> transactions);

    String getProduct();

    void showProgress(boolean show);

}
