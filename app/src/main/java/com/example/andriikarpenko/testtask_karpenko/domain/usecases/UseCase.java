package com.example.andriikarpenko.testtask_karpenko.domain.usecases;


import io.reactivex.Observable;

public interface UseCase<T> {

    Observable<T> execute();
}
