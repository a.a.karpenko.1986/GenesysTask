package com.example.andriikarpenko.testtask_karpenko.domain.repository;

import com.example.andriikarpenko.testtask_karpenko.domain.entities.Rate;
import com.example.andriikarpenko.testtask_karpenko.domain.entities.Transaction;

import java.util.List;


public interface Repository {

    List<Transaction> getTransactions();

    List<Rate> getRates();

}
