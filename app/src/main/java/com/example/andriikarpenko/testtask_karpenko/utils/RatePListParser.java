package com.example.andriikarpenko.testtask_karpenko.utils;

import com.example.andriikarpenko.testtask_karpenko.domain.entities.Rate;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class RatePListParser {

    public static List<Rate> parse(Document document) {
        final List<Rate> dataModels = new ArrayList<>();

        final NodeList nodes_array = document.getElementsByTagName("array");

        for (int index = 0; index < nodes_array.getLength(); index++) {

            final Node node = nodes_array.item(index);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                final Element e = (Element) nodes_array.item(index);

                final NodeList nodeKey = e.getElementsByTagName("key");
                final NodeList nodeString = e.getElementsByTagName("string");
                Rate rate = new Rate();

                for (int i = 0; i < nodeString.getLength(); i++) {

                    final Element eleKey = (Element) nodeKey.item(i);
                    final Element eleString = (Element) nodeString.item(i);

                    if (eleString != null) {
                        String strValue = getValue(eleString);

                        if (getValue(eleKey).equals("from")) {
                            rate = new Rate();
                            rate.setFrom(strValue);

                        } else if (getValue(eleKey).equals("rate")) {
                            rate.setRate(Float.valueOf(strValue));

                        } else if (getValue(eleKey).equals("to")) {
                            rate.setTo(strValue);
                            dataModels.add(rate);
                        }
                    }
                }
            }
        }
        return dataModels;
    }

    private static String getValue(Element item) {
        return item.getFirstChild().getNodeValue();
    }

}
