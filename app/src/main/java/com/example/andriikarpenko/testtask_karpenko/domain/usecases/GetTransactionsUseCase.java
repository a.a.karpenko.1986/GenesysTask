package com.example.andriikarpenko.testtask_karpenko.domain.usecases;


import com.example.andriikarpenko.testtask_karpenko.utils.CurrencyConverter;
import com.example.andriikarpenko.testtask_karpenko.domain.entities.Rate;
import com.example.andriikarpenko.testtask_karpenko.domain.repository.Repository;
import com.example.andriikarpenko.testtask_karpenko.domain.entities.Transaction;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public class GetTransactionsUseCase implements UseCase<List<Transaction>> {

    private Repository mRepository;
    private String mProduct;
    private String mCurrency;


    public GetTransactionsUseCase(Repository repository, String product, String currency) {
        this.mRepository = repository;
        this.mProduct = product;
        this.mCurrency = currency;
    }


    @Override
    public Observable<List<Transaction>> execute() {
        return Observable.fromCallable(() -> {
            List<Transaction> transactions = mRepository.getTransactions();
            List<Rate> rates = mRepository.getRates();
            CurrencyConverter converter = new CurrencyConverter(rates);

            List<Transaction> list = new ArrayList<>();
            for (Transaction transaction : transactions) {
                if (transaction.getSku().equals(mProduct)) {
                    float convertedAmount = converter.convert(transaction.getAmount(), transaction.getCurrency(), mCurrency);
                    transaction.setAmount(convertedAmount);
                    transaction.setCurrency(mCurrency);
                    list.add(transaction);
                }
            }

            return list;
        });

    }
}
