package com.example.andriikarpenko.testtask_karpenko.ui;

import java.util.List;


public interface MainView {

    void setProducts(List<String> products);

    void showProductInfo(String name, int transactionCount);

    void hideProductInfo();

    void showProgress(boolean show);

    void openProductDetails(String product);

}
