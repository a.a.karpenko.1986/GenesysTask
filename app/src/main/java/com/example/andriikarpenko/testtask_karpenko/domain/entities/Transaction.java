package com.example.andriikarpenko.testtask_karpenko.domain.entities;


public class Transaction {

    private float amount;
    private String currency;
    private String sku;

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public float getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getSku() {
        return sku;
    }
}
