package com.example.andriikarpenko.testtask_karpenko.presenters;


public interface Presenter<T>  {

    void attachView(T view);

    void onStart();

    void onStop();
}
