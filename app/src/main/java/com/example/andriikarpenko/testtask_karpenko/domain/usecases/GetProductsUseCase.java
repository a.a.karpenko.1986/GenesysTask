package com.example.andriikarpenko.testtask_karpenko.domain.usecases;


import com.example.andriikarpenko.testtask_karpenko.domain.repository.Repository;
import com.example.andriikarpenko.testtask_karpenko.domain.entities.Transaction;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;


public class GetProductsUseCase implements UseCase<List<String>> {

    private Repository repository;

    public GetProductsUseCase(Repository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<List<String>> execute() {

        return Observable.fromCallable(() -> {
            List<Transaction> transactions = repository.getTransactions();

            List<String> products = new ArrayList<>();
            for (Transaction transaction : transactions) {
                if (!products.contains(transaction.getSku())) {
                    products.add(transaction.getSku());
                }
            }
            return products;
        });
    }
}
