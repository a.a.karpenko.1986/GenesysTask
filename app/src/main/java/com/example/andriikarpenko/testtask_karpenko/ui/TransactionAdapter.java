package com.example.andriikarpenko.testtask_karpenko.ui;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.andriikarpenko.testtask_karpenko.R;
import com.example.andriikarpenko.testtask_karpenko.domain.entities.Transaction;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.TransactionItemHolder> {

    private List<Transaction> mTransactions = new ArrayList<>();

    @Override
    public TransactionAdapter.TransactionItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.transaction_item, parent, false);
        return new TransactionItemHolder(view);
    }

    @Override
    public void onBindViewHolder(TransactionItemHolder holder, int position) {
        Transaction transaction = mTransactions.get(position);

        holder.tvProductName.setText(transaction.getSku());
        holder.tvCurrency.setText(transaction.getCurrency());
        holder.tvAmount.setText(String.format(Locale.getDefault(), "%.2f", transaction.getAmount()));
    }

    @Override
    public int getItemCount() {
        return mTransactions.size();
    }

    class TransactionItemHolder extends RecyclerView.ViewHolder {

        TextView tvProductName;
        TextView tvAmount;
        TextView tvCurrency;

        public TransactionItemHolder(View itemView) {
            super(itemView);
            tvProductName = (TextView) itemView.findViewById(R.id.tvProductName);
            tvAmount = (TextView) itemView.findViewById(R.id.tvAmount);
            tvCurrency = (TextView) itemView.findViewById(R.id.tvCurrency);
        }
    }

    public void setItems(List<Transaction> items) {
        mTransactions.clear();
        mTransactions.addAll(items);
        notifyDataSetChanged();
    }
}
