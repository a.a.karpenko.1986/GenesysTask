package com.example.andriikarpenko.testtask_karpenko.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.andriikarpenko.testtask_karpenko.R;
import com.example.andriikarpenko.testtask_karpenko.presenters.MainPresenter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MainView {

    private MainPresenter presenter;
    private Spinner spProducts;
    private TextView tvProductName;
    private TextView tvTransactionCount;
    private ViewGroup vgProductHolder;
    private ProgressBar progressBar;
    private SpinnerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spProducts = (Spinner) findViewById(R.id.spinner);
        tvProductName = (TextView) findViewById(R.id.tvProductName);
        tvTransactionCount = (TextView) findViewById(R.id.tvTransactionsCount);
        vgProductHolder = (ViewGroup) findViewById(R.id.productHolder);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        vgProductHolder.setOnClickListener(v -> presenter.onProductClicked());

        initSpinner(new ArrayList<>());

        presenter = new MainPresenter();
        presenter.attachView(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.onStop();
    }

    @Override
    public void setProducts(List<String> products) {
        initSpinner(products);
    }

    @Override
    public void showProductInfo(String name, int transactionCount) {
        vgProductHolder.setVisibility(View.VISIBLE);
        tvProductName.setText(name);
        tvTransactionCount.setText(String.valueOf(transactionCount));
    }

    @Override
    public void hideProductInfo() {
        vgProductHolder.setVisibility(View.GONE);
    }

    @Override
    public void showProgress(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void openProductDetails(String product) {
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra(DetailsActivity.PRODUCT_KEY, product);
        startActivity(intent);
    }

    private void initSpinner(List<String> items) {
        items.add(getString(R.string.spinner_default_text));
        spProducts.setOnItemSelectedListener(null);

        adapter = new SpinnerAdapter(this, items, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spProducts.setAdapter(adapter);
        spProducts.setSelection(adapter.getCount(), false);
        spProducts.setOnItemSelectedListener(new EmptyOnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                presenter.onProductSelected(adapter.getItem(position));
            }
        });
    }

}
