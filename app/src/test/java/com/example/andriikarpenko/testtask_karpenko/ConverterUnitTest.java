package com.example.andriikarpenko.testtask_karpenko;

import com.example.andriikarpenko.testtask_karpenko.domain.entities.Rate;
import com.example.andriikarpenko.testtask_karpenko.utils.CurrencyConverter;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


public class ConverterUnitTest {

    private CurrencyConverter converter;

    @Test
    public void test_empty() {
        try {
            converter = new CurrencyConverter(new ArrayList<>());
            converter.convert(0.0f, "USD", "UAH");
        } catch (ArithmeticException e) {
            assertThat(e).hasMessage("Cannot calculate");
        }
    }

    @Test
    public void test_from_to_the_same() {
        List<Rate> rates = new ArrayList<>();
        rates.add(new Rate("USD", "GBP", 0.77f));

        converter = new CurrencyConverter(rates);
        float amount = converter.convert(10.0f, "USD", "USD");
        assertThat(amount).isEqualTo(10.0f);
    }

    @Test
    public void test_by_one_step() {
        List<Rate> rates = new ArrayList<>();
        rates.add(new Rate("USD", "GBP", 0.5f));

        converter = new CurrencyConverter(rates);
        float amount = converter.convert(10.0f, "USD", "GBP");
        assertThat(amount).isEqualTo(5.0f);
    }

    @Test
    public void test_by_two_step() {
        List<Rate> rates = new ArrayList<>();
        rates.add(new Rate("USD", "GBP", 2.0f));
        rates.add(new Rate("GBP", "CAD", 3.0f));

        converter = new CurrencyConverter(rates);
        float amount = converter.convert(10.0f, "USD", "CAD");
        assertThat(amount).isEqualTo(60.0f);
    }

    @Test
    public void test_by_three_step() {
        List<Rate> rates = new ArrayList<>();
        rates.add(new Rate("USD", "GBP", 2.0f));
        rates.add(new Rate("GBP", "CAD", 3.0f));
        rates.add(new Rate("CAD", "UAH", 2.0f));

        converter = new CurrencyConverter(rates);
        float amount = converter.convert(10.0f, "USD", "UAH");
        assertThat(amount).isEqualTo(120.0f);
    }

    @Test
    public void test_different_ways() {
        List<Rate> rates = new ArrayList<>();
        rates.add(new Rate("USD", "GBP", 2.0f));
        rates.add(new Rate("GBP", "CAD", 3.0f));
        rates.add(new Rate("GBP", "AUD", 10.0f));
        rates.add(new Rate("CAD", "UAH", 2.0f));

        converter = new CurrencyConverter(rates);
        float amount = converter.convert(10.0f, "USD", "UAH");
        assertThat(amount).isEqualTo(120.0f);
    }

    @Test
    public void test_first_way_wrong() {
        List<Rate> rates = new ArrayList<>();
        rates.add(new Rate("USD", "GBP", 2.0f));
        rates.add(new Rate("GBP", "AUD", 10.0f));
        rates.add(new Rate("GBP", "CAD", 3.0f));
        rates.add(new Rate("CAD", "UAH", 2.0f));

        converter = new CurrencyConverter(rates);
        float amount = converter.convert(10.0f, "USD", "UAH");
        assertThat(amount).isEqualTo(120.0f);
    }

    @Test
    public void test_wrong_ways() {
        List<Rate> rates = new ArrayList<>();
        rates.add(new Rate("USD", "GBP", 2.0f));
        rates.add(new Rate("GBP", "AUD", 10.0f));
        rates.add(new Rate("GBP", "CAD", 3.0f));
        rates.add(new Rate("AUD", "USD", 2.0f));
        rates.add(new Rate("CAD", "AUD", 2.0f));
        rates.add(new Rate("CAD", "UAH", 2.0f));

        converter = new CurrencyConverter(rates);
        float amount = converter.convert(10.0f, "USD", "UAH");
        assertThat(amount).isEqualTo(120.0f);
    }


}